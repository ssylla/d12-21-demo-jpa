package fr.digi.d12.demojpa;

import fr.digi.d12.demojpa.bo.Address;
import fr.digi.d12.demojpa.bo.Gender;
import fr.digi.d12.demojpa.bo.Profile;
import fr.digi.d12.demojpa.bo.User;
import org.jboss.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class App {
	
	public static void main( String[] args ) {
		
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory( "digi-d12" );
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();

		User user = new User( "ssy", "ssylla", "@ssylla" );
		user.setGender( Gender.M );
		Address address = new Address("10", "rue de la soif", "44000", "NTE");
		user.setAddress( address );
		Profile profile = new Profile("Admin");
		// profile.getUsers().add( user );
		// user.setProfile( profile );
		
		profile.addUser( user );
		em.persist( profile );
		
		
		em.getTransaction().commit();
		em.close();
		emf.close();
	}
}
