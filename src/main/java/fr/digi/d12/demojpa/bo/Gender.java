package fr.digi.d12.demojpa.bo;

public enum Gender {
	M("Male"), F("Female"), X("Trans");
	
	private String label;
	
	Gender( String label ) {
		this.label = label;
	}
	
	public String getLabel() {
		return label;
	}
	
	public void setLabel( String label ) {
		this.label = label;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "Gender{" );
		sb.append( "label='" ).append( label ).append( '\'' );
		sb.append( '}' );
		return sb.toString();
	}
}
