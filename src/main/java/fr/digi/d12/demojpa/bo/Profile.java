package fr.digi.d12.demojpa.bo;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="profil")
public class Profile implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "LIBELLE", length = 20)
	private String label;
	
	@OneToMany(mappedBy = "profile", cascade = CascadeType.ALL)//{CascadeType.PERSIST, CascadeType.MERGE, ...}
	private Set<User> users;
	
	{
		users = new HashSet<>();
	}
	
	public Profile() {
	}
	
	public Profile( String label ) {
		this.label = label;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId( Integer id ) {
		this.id = id;
	}
	
	public String getLabel() {
		return label;
	}
	
	public void setLabel( String label ) {
		this.label = label;
	}
	
	public Set<User> getUsers() {
		return users;
	}
	
	public void setUsers( Set<User> users ) {
		this.users = users;
	}
	
	public void addUser(User user) {
		user.setProfile( this );
	}
	
	public void removeUser(User user) {
		user.setProfile( null );
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "Profile{" );
		sb.append( "id=" ).append( id );
		sb.append( ", label='" ).append( label ).append( '\'' );
		sb.append( '}' );
		return sb.toString();
	}
}
