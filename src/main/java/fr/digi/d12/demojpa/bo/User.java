package fr.digi.d12.demojpa.bo;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "utilisateur")
public class User implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(length = 50)
	private String login;
	@Column(name = "MOT_DE_PASSE", length = 50)
	private String password;
	@Transient
	private String twitter;
	@Enumerated(EnumType.STRING)
	private Gender gender;
	@Embedded
	private Address address;
	
	@ManyToOne
	@JoinColumn(name = "ID_PROFIL")
	private Profile profile;
	
	public User() {}
	
	public User( String login, String password, String twitter ) {
		this.login = login;
		this.password = password;
		this.twitter = twitter;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId( Integer id ) {
		this.id = id;
	}
	
	public String getLogin() {
		return login;
	}
	
	public void setLogin( String login ) {
		this.login = login;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword( String password ) {
		this.password = password;
	}
	
	public String getTwitter() {
		return twitter;
	}
	
	public void setTwitter( String twitter ) {
		this.twitter = twitter;
	}
	
	public Profile getProfile() {
		return profile;
	}
	
	public void setProfile( Profile profile ) {
		if (this.profile != null) {
			this.profile.getUsers().remove( this );
		}
		this.profile = profile;
		if (this.profile != null) {
			this.profile.getUsers().add( this );
		}
	}
	
	public Gender getGender() {
		return gender;
	}
	
	public void setGender( Gender gender ) {
		this.gender = gender;
	}
	
	public Address getAddress() {
		return address;
	}
	
	public void setAddress( Address address ) {
		this.address = address;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "User{" );
		sb.append( "id=" ).append( id );
		sb.append( ", login='" ).append( login ).append( '\'' );
		sb.append( ", password='" ).append( password ).append( '\'' );
		sb.append( ", twitter='" ).append( twitter ).append( '\'' );
		sb.append( '}' );
		return sb.toString();
	}
}
